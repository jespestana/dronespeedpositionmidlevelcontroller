cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME droneSpeedPositionMidLevelController)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
#add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
add_definitions(-std=c++03)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries



set(DRONESPEEDPOSITIONMIDLEVELCONTROLLER_SOURCE_DIR
        src/source)

set(DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR
        src/include)

set(DRONESPEEDPOSITIONMIDLEVELCONTROLLER_HEADER_FILES
        ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR}/Controller_MidLevelCnt.h
        ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR}/Controller_MidLevel_SpeedLoop.h
        ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR}/config/config_controller_LinkQuad.h
        ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR}/config/config_controller_Parrot.h
        ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR}/config/config_controller_Pelican.h
)

set(DRONESPEEDPOSITIONMIDLEVELCONTROLLER_SOURCE_FILES
        ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_SOURCE_DIR}/Controller_MidLevelCnt.cpp
        ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_SOURCE_DIR}/Controller_MidLevel_SpeedLoop.cpp
)


find_package(catkin REQUIRED
		COMPONENTS lib_cvgutils lib_cvglogger
)

#OpenCV
find_package(OpenCV REQUIRED)


catkin_package(
        INCLUDE_DIRS ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR}
        LIBRARIES droneSpeedPositionMidLevelController
        DEPENDS OpenCV
        CATKIN_DEPENDS lib_cvgutils lib_cvglogger
  )


include_directories(${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_INCLUDE_DIR})
include_directories(${OpenCV_INCLUDE_DIRS})
include_directories(${catkin_INCLUDE_DIRS})


add_library(droneSpeedPositionMidLevelController ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_SOURCE_FILES} ${DRONESPEEDPOSITIONMIDLEVELCONTROLLER_HEADER_FILES})
add_dependencies(droneSpeedPositionMidLevelController ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneSpeedPositionMidLevelController ${OpenCV_LIBS})
target_link_libraries(droneSpeedPositionMidLevelController ${catkin_LIBRARIES})
